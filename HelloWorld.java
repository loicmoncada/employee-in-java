public class HelloWorld {
  public static void main(String[] args) {
    getEmployeeSalary();
    if (args.length == 1) {
      getEmployeeByPosition(args[0]);
    }
  }
  public static String[] employeeNames() {
    String[] eNames = new String[] { "Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly" };
    return eNames;
  }

  public static int[] hoursWorked() {
    int[] eNames = new int[] { 35, 38, 35, 38, 40 };
    return eNames;
  }

  public static double[] hourlyRates() {
    double[] eNames = new double[] { 12.5, 15.0, 13.5, 14.5, 13.0 };
    return eNames;
  }

  public static String[] positions() {
    String[] eNames = new String[] { "Caissier", "Projectionniste", "Caissier", "Manager", "Caissier" };
    return eNames;
  }

  public static String[] addX(int n, String arr[], String x) {
    int i;
    String newarr[] = new String[n + 1];
    for (i = 0; i < n; i++)
      newarr[i] = arr[i];
    newarr[n] = x;

    return newarr;
  }

  public static void getEmployeeByPosition(String position) {
    String[] results = {};
    for (int index = 0; index < positions().length; index++) {
      String pos = positions()[index];
      if (pos.equals(position)) {
        results = addX(results.length, results, employeeNames()[index]);
      }
    }
    if (results.length != 0) {
      for (String result : results) {
        System.out.println(result);
      }
    } else {
      System.out.println("Aucun employé trouvé.");
    }
  }

  public static void getEmployeeSalary() {
    double tauxHeureSupp = 1.5;
    for (int index = 0; index < positions().length; index++) {
      double salaire;
      if (hoursWorked()[index] > 35) {
        salaire = 35 * hourlyRates()[index] + (hoursWorked()[index] - 35) * tauxHeureSupp * hourlyRates()[index];
      } else {
        salaire = hoursWorked()[index] * hourlyRates()[index];
      }
      System.out.println(employeeNames()[index] + " Gagne: " + salaire + "€ en tant que " + positions()[index]);
    }
  }
}